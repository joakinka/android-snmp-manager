package com.example.kinka.androidsnmpmanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private TextView statusTextView;
    private FloatingActionButton fab;

    private Snmp snmp;
    private ResponseEvent response;

    private String myIP = "192.168.0.1"; //default
    private String ranger = "192.168.0."; //default
    private int startRange = 0;
    private int endRange = 255;
    private String community = "public";
    private String port = "/161";
    private int version = SnmpConstants.version1;

    private OID sysDescrOID =   new OID("1.3.6.1.2.1.1.1.0");
    private OID uptimeOID =     new OID("1.3.6.1.2.1.1.3.0");
    private OID sysNameOID =    new OID("1.3.6.1.2.1.1.5.0");
    private OID servicesOID =   new OID("1.3.6.1.2.1.1.7.0");

    private ArrayAdapter<String> listViewAdapter;
    private List<String> deviceList = new ArrayList<>();

    private List<Vector<VariableBinding>> varBindsList = new ArrayList<Vector<VariableBinding>>();
    private List<CommunityTarget> targetsIPList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statusTextView = (TextView)findViewById(R.id.statusTextView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getMyIP();
        setupListView();
        setTitle("Your IP is: " + myIP); //application title

        //Force to do the task in mainthread like this
        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy );

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AsyncScan().execute();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_range) {

            selectRangeDialog();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getMyIP() {
        try {
            Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface inet = (NetworkInterface) networkInterfaces.nextElement();
                Enumeration address = inet.getInetAddresses();
                while (address.hasMoreElements()) {
                    InetAddress inetAddress = (InetAddress) address.nextElement();
                    if (inetAddress.isSiteLocalAddress()) {
                        this.myIP = inetAddress.getHostAddress();
                        String[] ipSplit = myIP.split(Pattern.quote("."));
                        this.ranger = ipSplit[0] + "." + ipSplit[1] + "."  + ipSplit[2] + ".";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setupListView() {
        listView = (ListView) findViewById(R.id.list);

        listViewAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, deviceList);
        listView.setAdapter(listViewAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                showDeviceDetailDialog(position);
            }
        });
    }

    public void selectRangeDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Define a range of IPs to scan");
        builder.setMessage("The range must contain only numbers");

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText startEditText = new EditText(this);
        startEditText.setHint("Start");
        startEditText.setMaxWidth(3);
        startEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(startEditText);

        final EditText endEditText = new EditText(this);
        endEditText.setHint("End");
        endEditText.setMaxWidth(3);
        endEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(endEditText);

        builder.setView(layout);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                int value1 = Integer.parseInt(startEditText.getText().toString());
                int value2 = Integer.parseInt(endEditText.getText().toString());

                if (value1 < value2) {
                    startRange = value1;
                    endRange = value2;
                } else {
                    startRange = value2;
                    endRange = value1;
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void showDeviceDetailDialog(int position) {

        String name = "";
        String descr = "";
        String uptime = "";
        String services = "";

        Vector<VariableBinding> variables = varBindsList.get(position);
        String ip = targetsIPList.get(position).getAddress().toString();

        for (VariableBinding each : variables) {

            if (each.getOid().equals(sysNameOID)) {
                name = each.getVariable().toString();
            }

            if (each.getOid().equals(sysDescrOID)) {
                descr = each.getVariable().toString();
            }

            if (each.getOid().equals(uptimeOID)) {
                uptime = each.getVariable().toString();
            }

            if (each.getOid().equals(servicesOID)) {
                services = each.getVariable().toString();
            }
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setCancelable(true);
        dialog.setTitle(name + " " + ip);
        dialog.setMessage(descr + "\n\nUptime: "+ uptime + "\n\nServices running: " + services);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {}
        });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public CommunityTarget getTarget(String ip) {
        CommunityTarget target = new CommunityTarget();
        target.setCommunity(new OctetString(community)); //public
        target.setVersion(version); //v1
        target.setAddress(new UdpAddress(ip + port));
        target.setRetries(2);
        target.setTimeout(1000);
        return target;
    }

    public PDU getPDU(int id) {
        PDU protocolDataUnit = new PDU();
        protocolDataUnit.setType(PDU.GET);
        protocolDataUnit.setRequestID(new Integer32(id));
        protocolDataUnit.add(new VariableBinding(sysNameOID));
        protocolDataUnit.add(new VariableBinding(sysDescrOID));
        protocolDataUnit.add(new VariableBinding(uptimeOID));
        protocolDataUnit.add(new VariableBinding(servicesOID));
        return protocolDataUnit;
    }

    private class AsyncScan extends AsyncTask<String, Void, String> {

        private PDU pdu;
        private CommunityTarget target;

        @Override
        protected String doInBackground(String... params) {

            for(int i = startRange; i <= endRange; i++) {

                try {

                    snmp = new Snmp(new DefaultUdpTransportMapping());

                    snmp.listen();

                    this.pdu = getPDU(i);

                    target = getTarget(ranger + Integer.toString(i));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            statusTextView.setText("Trying to connect to " + target.getAddress());
                        }
                    }); //runOnUiThread

                    response = snmp.get(pdu, target);

                    if (response != null) {

                        PDU responsePDU = response.getResponse();

                        if (responsePDU != null) {

                            int errorStatus = responsePDU.getErrorStatus();

                            if (errorStatus == PDU.noError) {

                                final Vector<VariableBinding> varBinds = (Vector<VariableBinding>) responsePDU.getVariableBindings();

                                if (!targetsIPList.contains(target)) {

                                    targetsIPList.add(target);

                                    varBindsList.add(varBinds);

                                    for (final VariableBinding each : varBinds) {

                                        if (each.getOid().equals(sysNameOID)) {

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    listViewAdapter.add(each.getVariable().toString());
                                                }
                                            }); //runOnUiThread
                                        } //(each.getOid() == sysNameOID)
                                    } //(VariableBinding each : varBinds)
                                } //!targetsIPList.contains(target)
                            } //(errorStatus == PDU.noError)
                        } //(responsePDU != null)
                    } //(response != null)
                    snmp.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } //catch
            } //for(int i = startRange; i < endRange; i++)
            return "Executed";
        } //doInBackground

        @Override
        protected void onPostExecute(String result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    listViewAdapter.notifyDataSetChanged();
                    statusTextView.setText("Complete");
                    fab.setEnabled(true);
                }
            }); //runOnUiThread
        }

        @Override
        protected void onPreExecute() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    fab.setEnabled(false);
                }
            }); //runOnUiThread
        }

        @Override
        protected void onProgressUpdate(Void... values) {}

    }
}